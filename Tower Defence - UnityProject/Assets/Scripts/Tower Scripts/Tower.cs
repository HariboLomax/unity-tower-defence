using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class Tower : MonoBehaviour
{
    [Header("Referances")]
	[SerializeField] private Transform pointOfRotation;
	[SerializeField] private Transform towerCentre;
	[SerializeField] private LayerMask enemyLayer;
	[SerializeField] private GameObject projectilePrefab;
	[SerializeField] private Transform firingPoint;
	[SerializeField] public GameObject nextLevel;
	[SerializeField] public int upgradePrice;
	[SerializeField] public int towerPrice;
	[SerializeField] public bool maxUpgraded;
	[SerializeField] private GameObject upgradeSign;
	
    [Header("Attributes")]
	[SerializeField] private float range = 5f;
	[SerializeField] private float rpm = 60f; //Rounds per minute
	[SerializeField] private int towerDamage = 0; 
	
	private Transform currentTarget = null;
	
	private float timeUntilNextRound; 
	
	public void Awake(){
		//Without this the first enemy will just walk past.
		//This is because the Time.deltaTime is only added once an enemy is outve range.
		timeUntilNextRound =  60f / rpm;
	}
	
	private void Update(){
		//If the tower is not max ugraded
		if(!maxUpgraded){
			//And the player can afford the upgrade
			if (LevelManager.main.currency >= upgradePrice){
				//Make the upgrade sign visible to show the player they can upgrade
				upgradeSign.SetActive(true);
			}else{
				upgradeSign.SetActive(false);
			}
		}
		
		
		//If there is no current target look for a target
		if (currentTarget == null){
			FindNearestTarget();
		//If there is a current target rotate to face it	
		}else{
			pointOfRotation.rotation = Quaternion.Euler(new Vector3(0f,0f, RotationToCurrentTargetAngle() ));
		}
		
		//If there is no target in range then set the target to null
		if (!TargetStillInRange()){
			currentTarget = null;
		}else{
			//If there is a target in range reduce the RPM timer
			timeUntilNextRound += Time.deltaTime;
			if (timeUntilNextRound >= 60f / rpm){
				FireProjectile();
				timeUntilNextRound = 0;
			}
		}
	}
	
	//Function get the rotation to the current target
	private float RotationToCurrentTargetAngle(){
		//Differance in position
		Vector3 positionDifferance = (currentTarget.position - pointOfRotation.position);
		//Atan2 handles negatives and division by 0, gets the angle in radians 
		float angle = Mathf.Atan2(positionDifferance.y, positionDifferance.x)*Mathf.Rad2Deg;
		return angle;
	}
	
	//Function to fire a projectile towards a target
	private void FireProjectile(){
		//Create a new projectile
		GameObject projectile = Instantiate(projectilePrefab, firingPoint.position, Quaternion.identity);
		Projectile projectileScript = projectile.GetComponent<Projectile>();
		//Set the tower target and tower damage to the projectile
		projectileScript.SetNewTarget(currentTarget);
		projectileScript.SetDamage(towerDamage);
	}
	
	//Function to find the nearest target
	private void FindNearestTarget(){
		//2D circular raycast scan to find enemies in range
		RaycastHit2D[] enemiesInRange = Physics2D.CircleCastAll(towerCentre.position, range, (Vector2)towerCentre.position, 0f, enemyLayer);
		
		//If an enemy is detected in range 
		if (enemiesInRange.Length > 0){
			//set the current target to the enemy in range
			currentTarget = enemiesInRange[0].transform;
		}else{
			Debug.Log("NO TARGETS!");
		}
		
	}
	private bool TargetStillInRange(){
		if (currentTarget == null){return false;}
		return Vector2.Distance(currentTarget.position, towerCentre.position) <= range;
	}
	
	//Lines 108 - 111 are used to draw the aproximation of the raycast for debugging
	//Lines 108 - 111 work only in the editor (cannot be uncommented for the build)
	
	//private void OnDrawGizmosSelected(){
		//Handles.color = Color.cyan;
		//Handles.DrawWireDisc(towerCentre.position, towerCentre.forward, range);
	//}
	
	
}
