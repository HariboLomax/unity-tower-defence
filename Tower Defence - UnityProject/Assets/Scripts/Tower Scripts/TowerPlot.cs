using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerPlot : MonoBehaviour
{
	[Header("Referances")]
	[SerializeField] private SpriteRenderer spriteRenderer;
	[SerializeField] private Color hoverColor;
	
	private GameObject tower;
	private Color startColour;
	
	private void Start(){
		startColour = spriteRenderer.color;
	}
	
	//When the mouse enters set the colour to the highlight
	private void OnMouseEnter(){
		if (tower != null){return;}
		spriteRenderer.color = hoverColor;
	}
	
	//When the mouse leaves return the colour to normal
	private void OnMouseExit(){
		if (tower != null){return;}
		spriteRenderer.color = startColour;
	}
	
	//When the mouse is clicked
	private void OnMouseDown(){
		//Get the selected tower (useful for implementing multiple towers - not used due to time constraints)
		GameObject selectedTower = TowerBuilder.main.GetSelectedTower();
		//Get the selectedTower price
		int selectedTowerPrice = selectedTower.GetComponent<Tower>().towerPrice;
		//If there is no tower in this slot and the player can afford the selected tower
		if (tower == null && LevelManager.main.currency >= selectedTowerPrice){
			//Take the currency needed away
			LevelManager.main.currency -= selectedTowerPrice;
			//Get a copy of the tower prefab
			GameObject tempTower = TowerBuilder.main.GetSelectedTower();
			//Position modifier to ensure towers are placed correctly
			Vector2 posModifier = new Vector2(+1.6f,+0.5f);
			//Create new tower
			tower = Instantiate(tempTower, transform.position, Quaternion.identity);
			//Move it ontop of this plot
			tower.transform.Translate(posModifier);
			//Return plot to unhighlighted colour
			spriteRenderer.color = startColour;
		//If there is a tower here and the tower can be upgraded
		}else if (tower != null &&  tower.GetComponent<Tower>().maxUpgraded == false){
			//If the player can afford the upgrade
			if (LevelManager.main.currency >= tower.GetComponent<Tower>().upgradePrice){
				//Remove currency for upgrade
				LevelManager.main.currency -= tower.GetComponent<Tower>().upgradePrice;
				//Get a copy of the upgrade tower
				GameObject tempTower = tower.GetComponent<Tower>().nextLevel;
				//Destroy existing tower
				Destroy(tower);
				//Place new upgraded tower
				tower = Instantiate(tempTower, transform.position, Quaternion.identity);
				Vector2 posModifier = new Vector2(+1.6f,+0.5f);
				tower.transform.Translate(posModifier);
			}
		}
	}
}
