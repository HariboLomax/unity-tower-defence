using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Script to allow the building of different towers
//This script is currently only used for one tower due to time constraints
public class TowerBuilder : MonoBehaviour
{
	public static TowerBuilder main;
	
	
	[Header("Referances")]
	//Serializable field to enter differnent tower prefabs
	[SerializeField] private GameObject[] towerPrefabs;
	
	private int currentSelectedTower = 0;
	
	public void Awake(){
		main = this;
	}
	
	
	//If more towers were available this would be used to get teh current selected tower
	//If more towers were avaialble this value could be change via menus to select different towers to build
	public GameObject GetSelectedTower(){
		return towerPrefabs[currentSelectedTower];
	}
}
