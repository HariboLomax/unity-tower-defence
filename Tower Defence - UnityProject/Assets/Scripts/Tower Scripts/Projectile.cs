using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
	[Header("Referances")]
	[SerializeField] private Rigidbody2D rigidBody;
	
	[Header("Attributes")]
	[SerializeField] private float projectileSpeed = 5f;
	//How many degrees to adjust projectile to the RIGHT - Sometimes provided image is no rotated correctly.
	[SerializeField] private float angleModifier = 0f; 

	private Transform currentTarget;
	private int projectiledamage = 0;
	
	//Function to set a target, used to set the towers target to the projectile
	public void SetNewTarget(Transform newCurrentTarget){
		currentTarget = newCurrentTarget;
	}
	
	//Function to set damage, used to set the tower damage to the projectile
	public void SetDamage(int newDamage){
		projectiledamage = newDamage;
	}
	

	private void FixedUpdate(){
		//If there target does not exist any more (target was eliminated) then destroy the projectile - it no longer has a purpose
		if (!currentTarget){
			Destroy(gameObject);
			return;
		}
		//Caclulate the differance in position between the projectile and the target
		Vector3 positionDifferance = (currentTarget.position - transform.position);
		//Get the angle for the projectile to be rotated to, in order to face towards the target
		float angle = Mathf.Atan2(positionDifferance.y, positionDifferance.x)*Mathf.Rad2Deg;
		//Change the projectile rotation so that it faces the target
		transform.rotation = Quaternion.Euler(new Vector3(0f,0f, angle-angleModifier));
		//Get the normalised direction that the projectile must travel to reach the target
		Vector2 direction = (currentTarget.position - transform.position).normalized;
		//Increase the direction vector to ensure the projetile HITS the target, otherwise it will just follow behind the width of the projectille
		Vector2 increasedDirection = new Vector2((direction.x * 1.5f), (direction.y * 1.5f));
		//Close the distance between the projectile and the target based on the projectiles speed
		rigidBody.velocity = increasedDirection  * projectileSpeed;
	}
	
	//Triggered when the 2D collider detects a collision
	//The projectiles can only collide with the enemy layer
	private void OnCollisionEnter2D(Collision2D hit){
		//As projectiles can only collie with the enemy layer damage an enemy on hit
		hit.gameObject.GetComponent<EnemyController>().TakeDamage(projectiledamage);
		//Once the target is damage destroy the projectile
		Destroy(gameObject);
	}
	
}
