using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class LevelManager : MonoBehaviour{
    public static LevelManager main;
	
	[Header("Referances")]
	public Transform startPoint;
	public Transform[] pathChoice1;
	public Transform[] pathChoice2;
	public Dictionary<int,Transform[]> pathChoices;
	
	
	[Header("Attributes")]
	public int livesLeft = 1;
	public bool gameOver = false;
	public int wavesToSurvive = 2;
	public int currency = 0;
	public int thisLevel = 0;
	
	private void Awake(){
		main = this;
		//Dictionarys are not serializable. Turn the path choices into a dictionary.
		//Path choices being a dictionary means a random path choice can be chosen
		pathChoices = new Dictionary<int,Transform[]>(){{0,pathChoice1},{1,pathChoice2}};
	}
	
	//If the player loses all their lives set game over to ture, wavespawner will then end the game
	private void Update(){
		if (livesLeft <= 0) {
			gameOver = true;
		}
	
		
	}
}
