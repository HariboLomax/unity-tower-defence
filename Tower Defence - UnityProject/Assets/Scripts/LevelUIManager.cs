using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
public class LevelUIManager : MonoBehaviour
{
	[Header("Referances")]
	public TMP_Text livesText;
	public TMP_Text wavesLeftText;
	public TMP_Text currenyText;
	[SerializeField] private GameObject gameOverPanel;
	[SerializeField] private GameObject victoryPanel;
	[SerializeField] private GameObject saveNotDetected;
	[SerializeField] private GameObject victory;

	[Header("Events")]
	public static UnityEvent victoryEvent = new UnityEvent(); 
	public static UnityEvent gameOverEvent = new UnityEvent(); 

	private string currentSaveNameInput;
	
	//Create listeners for events
	private void Awake(){
		victoryEvent.AddListener(onVictory);
		gameOverEvent.AddListener(onGameOver);
	}
	
    //Update the text feilds to reflect the values
    void Update()
    {
		currenyText.SetText("Currency: " + LevelManager.main.currency);
		wavesLeftText.SetText("Waves: " + WaveSpawner.main.currentWave + " / " + LevelManager.main.wavesToSurvive);
		livesText.SetText("LIVES LEFT: " + LevelManager.main.livesLeft);
    }
	
	//Function to reset level upon clicking retry button
	public void RetryLevel(){
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+0);
	}
	
	//Function linked to the onvictory event - invoked from wave manager
	public void onVictory(){
		//Make victory panel visible
		victoryPanel.SetActive(true);
		//If no player data is currently in use, prompt the player to create a new save
		if (StaticData.fileSaveName == null){
			saveNotDetected.SetActive(true);
			victory.SetActive(false);
		//If player data is inuse save to the player data
		}else{
			victory.SetActive(true);
			saveNotDetected.SetActive(false);
		}
	}
	
	//Function to read the load input
	public void ReadStringInput(string s){
		currentSaveNameInput = s;
	}
	
	//Function to confirm the save name on selecting the load button
	public void ConfirmSaveName(){
		StaticData.fileSaveName = currentSaveNameInput;
		SaveSystem.SaveData(StaticData.fileSaveName, LevelManager.main.thisLevel);
		victory.SetActive(true);
		saveNotDetected.SetActive(false);
	}
	
	//Function upon clicking save and return to menu
	public void SaveAndReturnToMenu(){
		SaveSystem.SaveData(StaticData.fileSaveName, LevelManager.main.thisLevel);
		SceneManager.LoadScene("MenuScene");
	}
	
	//Function to return to menu
	public void ReturnToMenu(){
		SceneManager.LoadScene("MenuScene");
	}
	
	//Function  to bring up game panel when the game over event is invoked
	public void onGameOver(){
		gameOverPanel.SetActive(true);
	}
	
}
