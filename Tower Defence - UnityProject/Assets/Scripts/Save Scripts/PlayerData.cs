using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//I do not claim this save system as my own. 
//Lines 9 - 18 adapted from https://www.youtube.com/watch?v=XOjd_qU2Ido

[System.Serializable]
public class PlayerData
{
	public int levelsCompleted;
	
	public PlayerData(int newLevelsCompleted){
		levelsCompleted = newLevelsCompleted;
	}
	
}
