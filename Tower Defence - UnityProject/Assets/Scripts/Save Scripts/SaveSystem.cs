using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


//I do not claim this save system as my own. 
//Lines 11 - 40 adapted from https://www.youtube.com/watch?v=XOjd_qU2Ido

public static class SaveSystem
{
	//Saves PlayerData to a file in the persistant data path
   public static void SaveData(string saveName, int levelsCompleted){
	   BinaryFormatter formatter = new BinaryFormatter();
	   string path = Application.persistentDataPath + "/" + saveName + ".sav";
	   FileStream stream = new FileStream(path, FileMode.Create);
	   //Creates new player data to save
	   PlayerData data = new PlayerData(levelsCompleted);
	   //Formats the player data as a binary file and saves it
	   formatter.Serialize(stream, data);
	   //Close the file
	   stream.Close();
	   Debug.Log("Successfully saved, " + saveName + ", at " + path);
   }
   
   //Loads PlayerData from a file into the StaticData (StaticData persists accross scenes)
   public static PlayerData LoadData(string saveName){
	   string path = Application.persistentDataPath + "/" + saveName + ".sav";
	   //If the file exists
	   if (File.Exists(path)){
		   BinaryFormatter formatter = new BinaryFormatter();
		   //Open the previous beinary file 
		   FileStream stream = new FileStream(path, FileMode.Open);
		   //Turn the binary data back into player data
		   PlayerData data = formatter.Deserialize(stream) as PlayerData;
		   //Close the file
		   stream.Close();
		   return data; 
	   }else{
		   Debug.Log("Save file not found in " + path);
		   return null;
	   }
   }
}
