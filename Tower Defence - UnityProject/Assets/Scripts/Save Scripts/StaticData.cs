using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//StaticData contains static variables. Static variables can be accessed from any scene.

public class StaticData : MonoBehaviour
{
    public static string fileSaveName = null;
}
