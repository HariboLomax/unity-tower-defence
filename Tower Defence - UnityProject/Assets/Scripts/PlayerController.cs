using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
public class PlayerController : MonoBehaviour
{
	[Header("Referances")]
	[SerializeField] private Collider2D collider;
	[SerializeField] private LayerMask enemyLayer;
	
	
	[Header("Attributes")]
	public float movemenetSpeed;
	[SerializeField] private float attackLength = 0.25f;
	[SerializeField] private int attackDamage = 5;
	[SerializeField] private float attackRange = 2f;
	[SerializeField] private float attackCooldown = 0.1f;
	
	private float speedX,speedY;
	private Rigidbody2D rb;
	private Animator animator;
	private bool isFacingRight = true;
	private bool isAttacking = false;
	private float attackTime = 0;
	private bool onCoolDown = false;
	private float coolDownTime = 0;
	private Transform currentTarget = null;
	
    void Start()
    {
		//Set the rigid body 
        rb = GetComponent<Rigidbody2D>();
		//Set the animator
		animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
		//Set the x and y speed to the input * movement speed
        speedX = Input.GetAxisRaw("Horizontal") * movemenetSpeed;
		speedY = Input.GetAxisRaw("Vertical") * movemenetSpeed;
		//Set the players velocity to the x and y speed
		rb.velocity = new Vector2(speedX, speedY);
		//Update the animator blend tree to blend movement smoothly based on player speed
		float angularVelocity = Mathf.Sqrt(Mathf.Pow(rb.velocity.x,2) + Mathf.Pow(rb.velocity.y,2));
		animator.SetFloat("velocity", Mathf.Abs(angularVelocity));
		//Flip the character sprite based on directional speed
		if(speedX < 0 && isFacingRight == true){
			FlipDirection();
		}else if (speedX > 0 && isFacingRight == false){
			FlipDirection();
		}
		
		//If space is pressed attack
		if (Input.GetKeyDown(KeyCode.Space)){
			if (onCoolDown == false){
				attackInArea();
			}
		}
		
		//If the attack is on cooldown, tick down the timer until attack is no longer on cooldown
		if (onCoolDown == true){
			coolDownTime += Time.deltaTime;
			if (coolDownTime >= attackCooldown){
				onCoolDown = false;
				coolDownTime = 0;
			}
		}
		
		//If the sprite is attacking, remain attacking until the animation has finished playing
		if(isAttacking){
			attackTime += Time.deltaTime;
			if(attackTime >= attackLength){
				attackTime = 0;
				isAttacking = false;
				animator.SetBool("isAttacking", isAttacking);
			}
		}
		

    }
	
	//Constantly scan for enemies in range. Uses the same system as the Tower script.
	void FixedUpdate(){
		RaycastHit2D[] enemiesInRange = Physics2D.CircleCastAll(transform.position, attackRange, (Vector2)transform.position, 0f, enemyLayer);
		
		if (enemiesInRange.Length > 0){
			currentTarget = enemiesInRange[0].transform;
		}else{
			currentTarget = null;
		}
	}
	
	//Function to attack in an area
	void attackInArea(){
		//If there is a current target, damage the target
		if(currentTarget){
			currentTarget.GetComponent<EnemyController>().TakeDamage(attackDamage);
			currentTarget = null;
		}
		//Reset the attacking variables to start the cooldown
		isAttacking = true;
		onCoolDown = true;
		//Play animation
		animator.SetBool("isAttacking", isAttacking);
	}
	
	//Flip character based on the direction the character is facing
	void FlipDirection(){
		Vector3 flip = transform.localScale;
		flip.x *= -1f;
		transform.localScale = flip;
		isFacingRight = !isFacingRight;
	}
	
	//Debug code for drawning raycast area. Only works in editor, not in the build.
	
	//private void OnDrawGizmosSelected(){
		//Handles.color = Color.cyan;
		//Handles.DrawWireDisc(transform.position, transform.forward, attackRange);
	//}
}
