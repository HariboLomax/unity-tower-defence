using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuUIManager : MonoBehaviour
{
	
	
	[Header("Referances")]
	public GameObject canvas;
	public GameObject mainMenuPanel;
	public GameObject levelMenuPanel;
	public GameObject loadMenuPanel;
	public GameObject instructionMenuPanel;
	public GameObject levelButtons;
	public GameObject congrats;
	public GameObject nextPage;
	public GameObject previousPage;
	public GameObject[] instructionsList;
	
	private string currentSaveNameInput;
	private int pages;
	private int currentPage;
	
	//Upon awake set the amount of cyclable pages, set teh current page to 0
	private void Awake(){
		pages = instructionsList.Length;
		currentPage = 0;
		
	}
	
	//Open the menu upon start
	public void Start(){
		OpenMainMenuPanel();
	}
	
	//On update make the correct page button visible
	//Do not display next page if on last page
	//Dot no display previous page if on the first page
	public void Update(){
		if(currentPage + 1 == pages){
			nextPage.SetActive(false);
		}else if (currentPage == 0){
			previousPage.SetActive(false);
		}else{
			nextPage.SetActive(true);
			previousPage.SetActive(true);
		}
		
	}
    
	//Function to open the main menu
	public void OpenMainMenuPanel(){
		CloseAllPanels();
		mainMenuPanel.SetActive(true);
	}
	
	//Function to open the level menu
	public void OpenLevelMenuPanel(){
		CloseAllPanels();
		//If the player has saved data
		if(StaticData.fileSaveName != null){
			//Load the data
			PlayerData data = SaveSystem.LoadData(StaticData.fileSaveName);
			int count = 0;
			//For each level 
			foreach(Transform child in levelButtons.transform){
				string name = child.gameObject.name;
				Debug.Log("Child Name: " + name);
				int levelNumber = int.Parse(name);
				//If the level prior has been completed, unlock the level
				if (levelNumber <= data.levelsCompleted+1){
					child.gameObject.GetComponent<Button>().interactable = true;
					child.FindChild("Lock").gameObject.SetActive(false);
				}
				count += 1;
			}
			//If all levels are completed congratulate the player
			if (data.levelsCompleted >= count){
				congrats.SetActive(true);
			}
		}
		//Make the level menu panel visible
		levelMenuPanel.SetActive(true);
	}
	
	//Function to open the load menu
	public void OpenLoadMenuPanel(){
		CloseAllPanels();
		loadMenuPanel.SetActive(true);
	}
	
	//Function to open the instructions menu
	public void OpenInstructionsMenuPanel(){
		CloseAllPanels();
		instructionMenuPanel.SetActive(true);
		//For every panel in teh instructions list set to invisible
		foreach(GameObject child in instructionsList){
			child.SetActive(false);
		}
		currentPage = 0;
		previousPage.SetActive(false);
		nextPage.SetActive(true);
		//Make the first page in the list visible
		instructionsList[0].SetActive(true);
	}
	
	//Function to go to the next page in the instructions menu
	//These buttons do not exist on screen when out of bounds can occur
	public void NextPage(){
		Debug.Log("NXT PAGE!");
		instructionsList[currentPage].SetActive(false);
		currentPage += 1;
		instructionsList[currentPage].SetActive(true);
	}
	
	//Function to go to the previous page in the instructions menu
	//These buttons do not exist on screen when out of bounds can occur
	public void PreviousPage(){
		instructionsList[currentPage].SetActive(false);
		currentPage -= 1;
		instructionsList[currentPage].SetActive(true);
	}
	
	//Function to close all panels - prevents panels overlaying ontop of each other
	public void CloseAllPanels(){
		foreach(Transform child in canvas.transform){
			child.gameObject.SetActive(false);
		}
	}
	
	//Function to load level 1
	public void BeginLevel_1(){
		SceneManager.LoadScene("Level1Scene");
	}
	
	//Function to load level 2
	public void BeginLevel_2(){
		SceneManager.LoadScene("Level2Scene");
	}
	
	//Function to load level 3
	public void BeginLevel_3(){
		SceneManager.LoadScene("Level3Scene");
	}
	
	//Function to read current save input
	public void ReadStringInput(string s){
		currentSaveNameInput = s;
	}
	
	//Function to load current save based on save name input
	public void LoadAndReturnToMenu(){
		if(SaveSystem.LoadData(currentSaveNameInput) != null){
			Debug.Log("SAVE DATA EXISTS!");
			StaticData.fileSaveName = currentSaveNameInput;
			OpenMainMenuPanel();
		}else{
			Debug.Log("SAVE DATA DOESNT EXIST!");
		}
		
	}
}
