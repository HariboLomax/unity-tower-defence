using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour
{
	[Header("Referances")]
	[SerializeField] private Rigidbody2D rb;
	[SerializeField] private Slider slider;
	[SerializeField] private Animator animator;
	
	[Header("Attributes")]
	[SerializeField] private int health = 10;
	[SerializeField] private int maxHealth = 10;
    [SerializeField] private float moveSpeed = 2f;
	[SerializeField] private int currenyDropOnDeath = 0;
	
	private Transform target;
	private int pathIndex = 0;
	private int randomPathChoice;
	
	
	
	private void Start(){
		//Select a random path from the dictionary
		randomPathChoice = Random.Range(0,LevelManager.main.pathChoices.Count);
		Debug.Log(randomPathChoice);
		//Asign the first node in the chosen path as a starting target
		target = LevelManager.main.pathChoices[randomPathChoice][0];
	}
	

	private void Update(){
		//Update the health bar to constatly display hp / max hp
		UpdateHealthBar();
		
		//If the game is over delete all enemies
		if (LevelManager.main.gameOver == true){
			Destroy(gameObject);
		}
		
		//If the enemy has no health then
		if(health == 0){
			//Reduce the amount of enemies alive in the wavespawner
			WaveSpawner.onEnemyKilled.Invoke();
			//Give the player currecny from defeating the enemy
			LevelManager.main.currency += currenyDropOnDeath;
			//Destroy the enemy object
			Destroy(gameObject);
			return;
		}
		
		
		//If we are at the end of the current target, change to the next target in the transform array
		if (Vector2.Distance(target.position, transform.position) <= .1f){
			pathIndex++;
			//If we get to the end then
			if (pathIndex == LevelManager.main.pathChoices[randomPathChoice].Length){
				//Reduce the amount of enemies alive in the wavespawner
				WaveSpawner.onEnemyKilled.Invoke();
				//Reduce the players lives
				LevelManager.main.livesLeft -= 1;
				//Destroy the enemy
				Destroy(gameObject);
				return;
			}
			//If the enemy is not at the end, set the target to the next item in the array.
			else{
				target = LevelManager.main.pathChoices[randomPathChoice][pathIndex];
			}
		}
		
		
	}
	
	private void FixedUpdate(){
		//Move the enemy towards its destination
		Vector2 direction = (target.position - transform.position);
        float distanceToTarget = direction.magnitude;
		
		//Normalize the speed so that the slime doesnt slow down as it reaches the target
		//Lines 84 - 93 are not my code. Adapted from https://forum.unity.com/threads/can-someone-help-me-understand-the-difference-between-transform-position-and-rigidbody-velocity.848446/
        if (distanceToTarget == 0)
        {
            transform.position = target.position;
            rb.velocity = Vector2.zero;
        }
        else
        {
            rb.velocity = direction.normalized * moveSpeed;
        }
	}
	
	//Function to take damage, bound to the take damage event
	public void TakeDamage(int damageToTake){
		//If the enemy will take damage more than its health, set its health 0 -> prevents breaking the health slider
		if (damageToTake >= health){
			health = 0;
		}else{
			health -= damageToTake;
		}
	}
	
	//Function to update teh heatlh bar to health / max health
	public void UpdateHealthBar(){
		animator.SetInteger("Health", health);
		slider.value = (float) health/maxHealth;
	}
}
