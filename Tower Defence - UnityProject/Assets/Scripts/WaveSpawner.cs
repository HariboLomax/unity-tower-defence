using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WaveSpawner : MonoBehaviour
{
	public static WaveSpawner main;
	[Header("Referances")]
	[SerializeField] private GameObject[] enemyPrefab;
	
	
    [Header("Attributes")]
	[SerializeField] private int enemyBaseSpawnAmount = 5;
	[SerializeField] private float enemiesPerSecond = 1f;
	[SerializeField] private float waveDownTime = 5f;
	[SerializeField] private float levelscaling = 0.75f;
	
	
	[Header("Events")]
	public static UnityEvent onEnemyKilled = new UnityEvent(); 
	public int currentWave;
	private float enemyLastSpawnedTime;
	private int enemiesAlive;
	private int enemiesLeftToInstantiate;
	private bool enemiesAreSpawning = false;
	
	//Create listen for enemy killed event
	private void Awake(){
		onEnemyKilled.AddListener(EnemyKilled);
		main = this;
	}
	
	
	//When the level starts, start the first wave
	//Waves are coroutines to allow for wait()
	private void Start(){
		StartCoroutine(StartWave());
	}
	
	//When an enemy is killed, reduce the enemy alive value
	//This function can be invoked via event to keep track of enemies that are killed
	private void EnemyKilled(){
		enemiesAlive--;
	}
	
	
	private void Update(){
		//If enemies are not spawning then return
		if (!enemiesAreSpawning){return;}
		//If the game is over - game is lost
		if (LevelManager.main.gameOver == true ){
			//stop the next wave advancing
			//bring up game over UI
			LevelUIManager.gameOverEvent.Invoke();
			Debug.Log("GAME LOST!");
			return;
		//If this is the last wave and all enemies are defeated, and none left to spawn - game is won	
		}else if(currentWave == LevelManager.main.wavesToSurvive && enemiesAlive == 0 && enemiesLeftToInstantiate == 0){
			//stop the next wave advancing
			// bring up victory UI
			LevelUIManager.victoryEvent.Invoke();
			Debug.Log("GAME WON!");
			return;
		//If all enemies are dead and non left to spawn, end the wave
		}else{
			if(enemiesAlive == 0 && enemiesLeftToInstantiate == 0){
				EndWave();
			}
		}
		
		//increment enemy last spawned time
		enemyLastSpawnedTime += Time.deltaTime;
		//If the last enemy spawn was within the enemies per second, and there are enemies left to spawn then spawn enemy and rest the timer
		if (enemyLastSpawnedTime >= (1/enemiesPerSecond) && enemiesLeftToInstantiate > 0){
			InstantiateEnemy();
			enemyLastSpawnedTime = 0f;
		}
		
			
	}
	
	
	//Startwave coroutine
	private IEnumerator StartWave(){
		//Wait the wave down time
		yield return new WaitForSeconds(waveDownTime);
		//set enemies to spawn to true
		enemiesAreSpawning = true;
		//Calcualte the enemies to spawn in the next wave
		enemiesLeftToInstantiate = EnemiesPerWave();
	}
	
	//Function to end the wave
	private void EndWave(){
		//Set enemiue spawning to false, increment current wave, start a new wave
		enemiesAreSpawning = false;
		enemyLastSpawnedTime = 0f;
		
		currentWave++;
		StartCoroutine(StartWave());
	}
	
	//Function to instantaite a new enemy
	private void InstantiateEnemy(){
		//Adjust values to accomodate 
		enemiesLeftToInstantiate--;
		enemiesAlive++;
		GameObject enemyToSpawn = enemyPrefab[Random.Range(0,enemyPrefab.Length)];
		Instantiate(enemyToSpawn, LevelManager.main.startPoint.position, Quaternion.identity);
		
	}
	
	
	//Function to calcualte enemies per waves based on the difficutly multiplier
	private int EnemiesPerWave(){
		return Mathf.RoundToInt(enemyBaseSpawnAmount * Mathf.Pow(currentWave, levelscaling));
	}
}
