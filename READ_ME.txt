This is a short tower defence game containing 3 levels with a unique twist of a player character.
Instructions for gameplay are included within the game.

Tower Defence - DesktopBuild
-------------------------
This build is intended for use on Windows 10 or Windows 11 devices. Compatability is not ensured for other OS.
This build was developed using Unity Editor version 2022.3.11f1. This version of Unity is required to run the build.

Navigate into this directory and run the "Major Project.exe" to excecute the build.
Upon running, a fullscreen window scaled to the size of your monitor will appear, this is the final game.
To take the window out of full screen use alt+enter.
To close the game use alt+f4.


Tower Defence -UnityProject
-------------------------
This folder contains all the Unity Project files.
Some files have been removed due to size. Please see /Tower Defence - UnityProject/.gitignore for the excldued files.
This project was developed using Unity Editor 2022.3.11f1. You may experiance erros using a different version of the editor.
Unity Hub 3.8.0 was used to open this project.
